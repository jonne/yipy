# yipy -- the Yiddish language processing toolkit

## Idea

Yiddish language processing library with "batteries included". Supported tasks include orthographic classification, non-phonetic word detection, text normalization and transliteration -- all "off-the-shelf". Also comes with corpora and extensible class design for custom training.

In other words, let's totally violate the UNIX philosophy. :)

## Miscellaneous

- UniMorph has [Yiddish lemmas](https://github.com/unimorph/yid/blob/master/yid) that could let us integrate a MSD into Yiddish word objects

## Phased roadmap

### Phase 1: Text classification I

The goals for this phase are to build an automated test infrastructure,
get to practice some CI and also write some object oriented NLP code for feature extraction.

- [x] Overall test infrastructure
- [x] Write text data interfaces (using spaCy?)
```
- yipy
    - yipy.base.Token
        - [x] test
    - yipy.base.WordCorpus
        - [x] corpus parsing from csv
        - [x] stateful: summary statistics for orthography breakdown
        - [x] stateful: has tags for orthography of each word in it
        - [x] tests
- yipy.datasets
    - [] yipy.datasets.LRECNounCorpus
```

- [] Orthographic classification classes + tests + pretrained model
```
- yipy.base
    - yipy.clf
        - ~~yipy.clf.AbstractClassifier~~ => `sklearn.base.BaseEstimator`
        - yipy.clf.RuleBasedClassifier
        - yipy.clf.OrthographyClassifier
            - supports underlying models: {maxent, nn, ...}
            - implemented using scikit-learn / skorch
    - yipy.feature_extraction
        - [x] yipy.feature_extraction.Feature
        - [x] yipy.feature_extraction.FeatureExtractor
        - [x] yipy.feature_extraction.NGramFeature
        - [x] yipy.feature_extraction.DefaultFeatureSet
- yipy.pretrained.clf
    - yipy.pretrained.clf.OrthographyClassifier
```

- [] Guild + Orthographic classification experiments with custom features
    - Development corpus

- [~] Text classification CLI (`yipy classify`)
```
- yipy.cli
    - usage: yipy classify [OPTIONS] < input_word_per_line.txt > output_word_per_line.txt
```



### Phase 2: Text classification II
- [] Corpus annotation for Non-phonetic spelling detection with Doccano
- [] Semi-supervised Non-phonetic spelling detection experiments & corpus annotation
    - Use FlyingSquid here
```
- yipy.datasets
    - yipy.datasets.NonPhoneticSpellingIdentificationCorpus
- yipy.clf
    - yipy.clf.NonPhoneticSpellingDetector
        - supports underlying models: {mnb, svm, maxent, nn, ...}
- yipy.rule_based.Rule
    - rule for regular expression
    - rule parsing from text
```

- [] Non-phonetic spelling detection corpus classes + pretrained model
```
- yipy.pretrained.clf
    - yipy.pretrained.clf.NonPhoneticSpellingDetector
```

### Phase 3: Transduction & text normalization
- [] Text normalization experiments with noun corpus & full wiktionary corpus
- [] Transducer classes + pretrained model

```
- yipy.datasets
    - yipy.datasets.WiktionaryCorpus
- yipy.base
    - yipy.feature_extraction
        - yipy.feature_extraction.SequenceFeatureExtractor
    - yipy.transducers
        - yipy.transducers.AbstractTextTransducer
        - yipy.transducers.RuleBasedFST
        - yipy.transducers.NeuralTransducer
        - yipy.transducers.CRFTransducer
        - yipy.transducers.HMMTransducer
    - yipy.transliterate
    	- yipy.transliterate.OrthographyStandardizer
    		- supports underlying models: {wfst, hmm, nn, ...}
    - yipy.pretrained
        - yipy.pretrained.standardize
        	- yipy.pretrained.standardize.TextStandardizer
    - yipy.cli
    	- usage: yipy standardize [OPTIONS] < input_sentence_per_line > output_sentence_per_line.txt
    	- usage: yipy transliterate [OPTIONS] < input_sentence_per_line > output_sentence_per_line.txt
```

### Phase 4: Language models & FSTs
- Fetch & normalize entire Wikipedia
- Fetch & normalize In Geveb data
- Train language models & FST based models

```
- yipy.base
    - yipy.lm
        - yipy.lm.AbstractLM
        - yipy.lm.NGramLanguageModel
        - yipy.lm.FSTLanguageModel
- yipy.pretrained
    - yipy.pretrained.lm
            - yipy.pretrained.lm.WiktionaryLM
            - yipy.pretrained.lm.InGevebLM
```

## Workflow

- Settle on idea to tackle in next "sprint"
- Perform preliminary experiments to decide on model & hyperparameter values
- Implement abstract class / interface if it doesn't exist yet
- Implement class that puts the idea to practice
- Write tests, verify they pass & verify coverage

## Experiments / Benchmarking

- Normalization via end-to-end models (NNs) vs multi-pass normalization
- Data set reduction to achieve similar performance as baseline
- Nearest neighbor search for closest words in other orthography
