from typing import Union, Tuple, List, Set, Dict
from base import (
    YiddishDiacritics,
    Token,
    roman_letters_wpunc,
)
import pandas as pd
import string


# Individual features
class Feature:
    def extract(self, word: Token, features: Dict[str, Union[float, int]]) -> None:
        """Main function of every feature. Note: modifies the `features` dict in place."""
        raise NotImplementedError


class BiasFeature(Feature):
    def extract(self, word: Token, features: Dict[str, Union[float, int]]) -> None:
        features["bias"] = 1.0


class TokenFeature(Feature):
    def extract(self, word: Token, features: Dict[str, Union[float, int]]) -> None:
        features[f"token={word.text}"] = 1.0


class BagOfCharactersFeature(Feature):
    def extract(self, word: Token, features: Dict[str, Union[float, int]]) -> None:
        for c in word.text:
            features[f"char={c}"] = 1.0


class SuffixFeature(Feature):
    def __init__(self, suffix_length: int = 3):
        self.suffix_length = suffix_length

    def extract(self, word: Token, features: Dict[str, Union[float, int]]) -> None:
        features[f"suffix={word.text[-self.suffix_length:]}"] = 1.0


class PrefixFeature(Feature):
    def __init__(self, prefix_length: int = 3):
        self.prefix_length = prefix_length

    def extract(self, word: Token, features: Dict[str, Union[float, int]]) -> None:
        features[f"prefix={word.text[:self.prefix_length]}"] = 1.0


class ContainsDiacriticsFeature(Feature):
    def __init__(self):
        self.yi = YiddishDiacritics()

    def extract(self, word: Token, features: Dict[str, Union[float, int]]) -> None:
        if self.contains_diacritics(word):
            features["contains_diacritics"] = 1.0

    def contains_diacritics(self, word: Token) -> int:
        """Does word w contain diacritics?"""
        return int(any(c in self.yi.diacritics for c in word))


class NeedsDiacriticsFeature(Feature):
    def __init__(self):
        self.yi = YiddishDiacritics()

    def extract(self, word: Token, features: Dict[str, Union[float, int]]) -> None:
        if self.no_diacritics_but_should_have(word):
            features["needs_diacritics"] = 1.0

    def no_diacritics_but_should_have(self, word: Token):
        """Rule: no diacritics but at least one character requires one"""
        no_diacritics = all(
            c not in self.yi.diacritics for c in word if c not in string.punctuation
        )
        needs_diacritics = any(c in self.yi.diacritics_per_letter for c in word)
        return int(no_diacritics and needs_diacritics)


class ContainsRomanFeature(Feature):
    def __init__(self):
        self.yi = YiddishDiacritics()

    def extract(self, word: Token, features: Dict[str, Union[float, int]]) -> None:
        if self.contains_roman_characters(word):
            features["contains_roman"] = 1.0

    def contains_roman_characters(self, word: Token) -> int:
        return int(any(c in roman_letters_wpunc for c in word))


class NgramFeature(Feature):
    def __init__(self, n: int = 3):
        self.n = n

    def extract(self, word: Token, features: Dict[str, Union[float, int]]) -> None:
        for gram in self.ngrams(word):
            features[f"ngram={gram}"] = 1.0

    def ngrams(self, word: Token):
        for gram in zip(*[word[j:] for j in range(self.n)]):
            yield "".join(gram)


# General feature extractor class
class FeatureExtractor:
    def __init__(self, features: List[Feature]):
        self.features = features

    def extract(self, word):
        """Main feature extraction loop. Applies each Feature to the word and returns the dict representation"""
        features = {}
        for f in self.features:
            f.extract(word, features)
        return features


class DefaultFeatureSetExtractor(FeatureExtractor):
    def __init__(
        self, prefix_length: int = 3, suffix_length: int = 3, ngram_length: int = 3
    ):
        features = [
            BiasFeature(),
            TokenFeature(),
            BagOfCharactersFeature(),
            SuffixFeature(suffix_length),
            PrefixFeature(prefix_length),
            ContainsRomanFeature(),
            ContainsDiacriticsFeature(),
            NeedsDiacriticsFeature(),
            NgramFeature(ngram_length),
        ]
        super().__init__(features)


# OLD CUSTOM FEATURE EXTRACTOR


class CustomFeatureExtractor:
    def __init__(self, prefix_length=3, suffix_length=3):
        self.yd = YiddishDiacritics()
        self.prefix_length = prefix_length
        self.suffix_length = suffix_length

    def extract(self, w: str) -> dict:
        """Extracts word features into a dictionary"""
        features = {"bias": 1.0}

        # feature 0: token itself
        features[f"token={w}"] = 1.0

        # feature 1: binary features for each char
        for c in w:
            features[f"char={c}"] = 1.0

        # feature 2: contains diacritics?
        features["contains_diacritics"] = self.contains_diacritics(w)

        # # feature 3: contains roman characters?
        features["contains_roman"] = self.contains_roman_characters(w)

        # feature 4: suffix
        suffix = self.suffix(w, n=self.prefix_length)
        features[f"suffix={suffix}"] = 1

        # feature 5: prefix
        prefix = self.prefix(w, n=self.suffix_length)
        features[f"prefix={prefix}"] = 1

        # feature 6: no diacritics but should have
        features["needs_diacritics"] = self.no_diacritics_but_should_have(w)

        return features

    def no_diacritics_but_should_have(self, word: str):
        """Rule: no diacritics but at least one character requires one"""
        yi = YiddishDiacritics()
        no_diacritics = all(
            c not in yi.diacritics for c in word if c not in string.punctuation
        )
        needs_diacritics = any(c in yi.diacritics_per_letter for c in word)
        return int(no_diacritics and needs_diacritics)

    def contains_diacritics(self, w: str) -> int:
        """Does word w contain diacritics?"""
        return int(any(c in self.yd.diacritics for c in w))

    def contains_roman_characters(self, w: str) -> int:
        return int(any(c in roman_letters_wpunc for c in w))

    def prefix(self, w: str, n: int = 3):
        return w[:n]

    def suffix(self, w: str, n: int = 3):
        return w[-n:]
