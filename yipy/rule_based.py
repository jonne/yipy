from sklearn.base import BaseEstimator
from collections import Counter
from typing import List, Any
import re


class Rule:
    def __init__(self, lhs: str, rhs: str, unk_symbol: str = "UNK") -> None:
        self._lhs = lhs
        self._rhs = rhs
        self.unk_symbol = unk_symbol

    def match(self, string: str) -> str:
        m = re.search(self._lhs, string)
        return self._rhs if m else self.unk_symbol


class RuleBasedClassifier(BaseEstimator):
    def __init__(
        self,
        rule_strings: List[str],
        rule_separator: str = " -> ",
        unk_symbol: str = "UNK",
    ) -> None:
        """Initializes a RuleBasedClassifier

        rules: str
            contents of a rules file, which specifies
            text-based rules and the corresponding classes

        Notes: rules specified as

            [regex] -> [class]

        so, for example, the rule

            .*ung$ -> romanized

        specifies that a string ending in -ung
        will be classified as being romanized.
        """
        self.rule_separator = rule_separator
        self.unk_symbol = unk_symbol
        self.rules = [r for r in self.parse_rules(rule_strings)]

    def parse_rules(self, rules: List[str]) -> List[Rule]:
        """
        Processes classification rules to dict representation
        """
        parsed = []
        for rule in rules:
            lhs, rhs = rule.split(self.rule_separator)
            parsed.append(Rule(lhs, rhs))

        return parsed

    def classify(self, s: str) -> str:
        """
        Majority-voting based classification
        """
        counts = Counter(
            rule.match(s) for rule in self.rules if rule.match(s) is not self.unk_symbol
        )
        if not counts:
            return self.unk_symbol

        return max(counts, key=lambda s: counts[s])

    @classmethod
    def from_text_file(
        cls, path: str, rule_separator: str = " -> ", unk_symbol: str = "UNK"
    ):
        with open(path, encoding="utf-8") as f:
            rule_lines = [rule.strip() for rule in f.readlines() if rule.strip()]
            return cls(
                rule_strings=rule_lines,
                rule_separator=rule_separator,
                unk_symbol=unk_symbol,
            )
