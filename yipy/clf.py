from sklearn.base import BaseEstimator
from sklearn.linear_model import LogisticRegression
from typing import Union
import numpy as np


class OrthographyClassifier(BaseEstimator):
    def __init__(
        self,
        yivo_clf: BaseEstimator,
        rom_clf: BaseEstimator,
        chasid_clf: BaseEstimator,
        unk_symbol: str = "UNK",
    ):
        self.yivo_clf = yivo_clf
        self.rom_clf = rom_clf
        self.chasid_clf = chasid_clf
        self.unk_symbol = unk_symbol

    def predict(self, X):
        y_pred_yivo = self.yivo_clf.predict(X)
        y_pred_rom = self.rom_clf.predict(X)
        y_pred_chasid = self.chasid_clf.predict(X)
        predictions = [
            self._decide(yivo, rom, chasid)
            for yivo, rom, chasid in zip(y_pred_yivo, y_pred_rom, y_pred_chasid)
        ]
        return np.array(predictions)

    def _decide(
        self, yivo: Union[bool, int], rom: Union[bool, int], chasid: Union[bool, int]
    ):
        if rom:
            return "ROM"
        elif yivo:
            return "YIVO"
        elif chasid:
            return "CHA"
        else:
            return self.unk_symbol
