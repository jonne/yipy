from dataclasses import dataclass, field
from collections import Counter
from typing import Sequence, Any, Union, List, Set
import pandas as pd
import string
import nltk

# Useful constants
roman_letters = set(string.ascii_letters)
roman_letters_wpunc = roman_letters.union(set(string.punctuation))

# Classes to hold predictoins
class ContingencyTable:
    def __init__(self, pos_label: Union[str, int, bool]):
        self.pos_label: Union[str, int, bool] = pos_label
        self.n_tp: int = 0
        self.n_fp: int = 0
        self.n_tn: int = 0
        self.n_fn: int = 0
        self.true_positives: List[str] = []
        self.false_positives: List[str] = []
        self.false_negatives: List[str] = []
        self.true_negatives: List[str] = []

    def add(
        self,
        instance: str,
        true_label: Union[str, int, bool],
        predicted_label: Union[str, int, bool],
    ):
        if true_label == self.pos_label:
            if true_label == predicted_label:
                self.n_tp += 1
                self.true_positives.append(instance)
            else:
                self.n_fn += 1
                self.false_negatives.append(instance)
        else:
            if true_label == predicted_label:
                self.n_tn += 1
                self.true_negatives.append(instance)
            else:
                self.n_fp += 1
                self.false_positives.append(instance)

    def __repr__(self):
        return f"ContingencyTable(n_tp={self.n_tp}, n_fp={self.n_fp}, n_tn={self.n_tn}, n_fn={self.n_fn})"


class ClassificationInstanceCounter:
    def __init__(
        self,
        instances: List[str],
        true_labels: List[Union[str, int, bool]],
        predicted_labels: List[Union[str, int, bool]],
    ):
        self.labels = self.get_unique_labels(true_labels)
        self.count_instances(instances, true_labels, predicted_labels)

    def get_unique_labels(
        self, true_labels: List[Union[str, int, bool]]
    ) -> Set[Union[str, int, bool]]:
        """Finds all unique labels that occur in a dataset"""
        return set(true_labels)

    def count_instances(self, instances, true_labels, predicted_labels):
        """Bucketizes each example into its correct bucket
        TODO: write a better docstring
        """
        if hasattr(self, "instance_counts"):
            return self.instance_counts
        self.instance_counts = {
            label: ContingencyTable(pos_label=label) for label in self.labels
        }
        for x, y_true, y_pred in zip(instances, true_labels, predicted_labels):
            self.instance_counts[y_true].add(x, y_true, y_pred)
            if y_true != y_pred:
                self.instance_counts[y_pred].add(x, y_true, y_pred)
        return self.instance_counts

    def true_positives(self, label: Union[str, int, bool], n: int = 0):
        if not n:
            return self.instance_counts[label].true_positives
        else:
            return self.instance_counts[label].true_positives[:n]

    def false_positives(self, label: Union[str, int, bool], n: int = 0):
        if not n:
            return self.instance_counts[label].false_positives
        else:
            return self.instance_counts[label].false_positives[:n]

    def true_negatives(self, label: Union[str, int, bool], n: int = 0):
        if not n:
            return self.instance_counts[label].true_negatives
        else:
            return self.instance_counts[label].true_negatives[:n]

    def false_negatives(self, label: Union[str, int, bool], n: int = 0):
        if not n:
            return self.instance_counts[label].false_negatives
        else:
            return self.instance_counts[label].false_negatives[:n]


class NLTKTokenizer:
    """Default tokenizer using word_tokenize() from NLTK"""

    def tokenize(self, line) -> List[str]:
        return nltk.word_tokenize(line)


@dataclass(frozen=False)
class Token:
    """Container class representing a token.
    Every Yiddish token will have an orthography 
    and optionally a dict of features."""

    text: str
    orthography: Union[str, None] = None
    features: Any = None

    @property
    def has_features(self) -> bool:
        return self.features is not None

    def __iter__(self):
        for c in self.text:
            yield c

    def __getitem__(self, ix):
        return self.text[ix]

@dataclass(frozen=False)
class Line:
    """Container class representing a line/sentence in a corpus.
    Can optionally have sentence-level features."""

    tokens: List[Token]
    features: Any = None

    def __getitem__(self, ix: int) -> Token:
        return self.tokens[ix]

    @property
    def has_features(self) -> bool:
        return self.features is not None


@dataclass(frozen=False)
class WordCorpus:
    """Corpus of words. Initializable from CSV"""

    words: List[Token]

    @classmethod
    def from_csv(cls, path: str, word_col: str = "word", orth_col: str = "orthography"):
        """Reads in a corpus from CSV. By default, expects the CSV
        file to have two columns 'word' and 'orthography'. These names
        may be overridden by setting 'word_col' and 'orth_col' as desired.
        """
        csv_file: pd.DataFrame = pd.read_csv(path, encoding="utf-8")
        words: pd.Series = csv_file[word_col]
        orths: pd.Series = csv_file[orth_col]
        return cls(words=[Token(text=w, orthography=o) for w, o in zip(words, orths)])

    def __getitem__(self, ix: int) -> Token:
        return self.words[ix]

    @property
    def orthography_statistics(self) -> Counter:
        return Counter(w.orthography for w in self.words)


class YiddishDiacritics:
    """General container for Yiddish diacritics."""

    def __init__(self):
        self.vovvov = "וו"
        self.tsvey_vovn = "װ"
        self.yudyud = "יי"
        self.tsvey_yudn, self.pasekh = "ײַ"
        self.pasekh_tsvey_yudn = self.tsvey_yudn + self.pasekh
        self.vovyud_separate = "וי"
        self.vovyud_joined = "ױ"
        (self.alef, self.komets, self.beys, self.veys_fey_diacritic) = "אָבֿ"
        self.pey, self.pey_diacritic = "פּ"
        self.sof, self.tof_diacritic = "תּ"
        self.vov, self.vov_u_diacritic = "וּ"
        self._diacritics = set(
            [
                self.pasekh,
                self.komets,
                self.veys_fey_diacritic,
                self.pey_diacritic,
                self.tof_diacritic,
                self.vov_u_diacritic,
            ]
        )

        self._required_diacritics = {
            self.alef: [self.pasekh, self.komets],
            self.beys: [self.veys_fey_diacritic],
            self.pey: [self.pey_diacritic, self.veys_fey_diacritic],
            self.vov: [self.vov_u_diacritic],
            self.sof: [self.tof_diacritic],
        }

    @property
    def diacritics(self) -> set:
        """Returns a set of diacritics."""
        return self._diacritics

    @property
    def diacritics_per_letter(self) -> dict:
        """Returns a letter -> diacritics mapping."""
        return self._required_diacritics
