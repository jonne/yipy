install_conda:
	conda create -n yipy python=3.8
	conda activate yipy
	pip install -r requirements.txt

install:
	pip install -r requirements.txt

tests:
	python -m unittest --verbose
