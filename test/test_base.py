# test_base.py

import os
import uuid
import unittest
from collections import Counter
from yipy.base import Token, Line, WordCorpus, NLTKTokenizer


def random_uuid() -> str:
    return str(uuid.uuid4())


class TestToken(unittest.TestCase):
    def test_token_init(self):
        """Tokens should be initializable and have no features."""
        token = Token(text="aksl", orthography="romanized")
        self.assertEqual("aksl", token.text)
        self.assertEqual("romanized", token.orthography)
        self.assertEqual(False, token.has_features)

    def test_featurized_token(self):
        """Tokens with features should be initializable."""
        dummy_features = {"first_char": "a", "last_char": "l"}
        token = Token(text="aksl", orthography="romanized", features=dummy_features)
        self.assertEqual("aksl", token.text)
        self.assertEqual("romanized", token.orthography)
        self.assertEqual(dummy_features, token.features)

    def test_token_later_featurization(self):
        """Tokens should be able to take on features after initialization, too."""
        dummy_features = {"first_char": "a", "last_char": "l"}
        token = Token(text="aksl", orthography="romanized")
        self.assertEqual("aksl", token.text)
        self.assertEqual("romanized", token.orthography)
        token.features = dummy_features
        self.assertEqual(dummy_features, token.features)


class TestLine(unittest.TestCase):
    def test_line_initialization(self):
        """Lines should be initializable and have no features by default"""
        sent = "I am Lorde"
        tokens = [Token(text=w) for w in sent.split()]
        line = Line(tokens=tokens)
        str_rep = "Line(tokens=[Token(text='I', orthography=None, features=None), Token(text='am', orthography=None, features=None), Token(text='Lorde', orthography=None, features=None)], features=None)"
        self.assertEqual(str_rep, str(line))
        self.assertEqual(False, line.has_features)

    def test_line_featurization(self):
        """TODO"""
        pass

    def test_grab_single_token(self):
        """It should be possible to grab single Tokens from a Line"""
        line = Line(tokens=[Token(text=t) for t in "I am Lorde ya ya ya".split()])
        tok = line[0]
        self.assertEqual("I", tok.text)


class TestWordCorpus(unittest.TestCase):
    def test_load_lrec_noun_corpus_absolutepath(self):
        csv_path = os.path.abspath("./data/yi-lrec2020-noun-corpus-orth-clf.csv")
        lrec = WordCorpus.from_csv(csv_path)
        althoykhdaytsh = lrec[123]
        self.assertEqual("althoykhdaytsh", althoykhdaytsh.text)
        self.assertEqual("romanized", althoykhdaytsh.orthography)
        self.assertEqual(None, althoykhdaytsh.features)

    def test_corpus_statistics(self):
        csv_path = os.path.abspath("./data/yi-lrec2020-noun-corpus-orth-clf.csv")
        lrec = WordCorpus.from_csv(csv_path)
        expected_counts = Counter({"romanized": 2750, "yivo": 2750, "chasidic": 2750})
        self.assertEqual(expected_counts, lrec.orthography_statistics)


class TestNLTKTokenizer(unittest.TestCase):
    def setUp(self):
        self.tokenizer = NLTKTokenizer()

    def test_tokenize(self):
        sent = "I am Carol"
        tokens = self.tokenizer.tokenize(sent)
        self.assertEqual(["I", "am", "Carol"], tokens)


if __name__ == "__main__":
    unittest.main()
