import pandas as pd
import click

# Quick script to get OOV words from complete Wiktionary corpus,
# as compared to the Yiddish noun corpus published by Saleva (2020).


@click.command()
@click.option("--noun-corpus-path", help="Path to Yiddish noun corpus CSV file.")
@click.option("--wiktionary-corpus-path", help="Path to Wiktionary corpus CSV file.")
@click.option("--output-path", help="Output path for Wiktionary OOV corpus.")
def main(noun_corpus_path: str, wiktionary_corpus_path: str, output_path: str):
    noun_corpus = pd.read_csv(noun_corpus_path, encoding="utf-8")
    wiktionary_corpus = pd.read_csv(wiktionary_corpus_path, encoding="utf-8")
    merged = pd.merge(
        wiktionary_corpus,
        noun_corpus,
        left_on="transliteration",
        right_on="romanized",
        how="left",
    )
    oov = merged[merged.yivo.isna()].copy()[["yiddish", "transliteration"]]
    oov.to_csv(output_path, index=False, encoding="utf-8")


if __name__ == "__main__":
    main()
