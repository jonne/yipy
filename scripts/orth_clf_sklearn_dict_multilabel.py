from sklearn.linear_model import LogisticRegression
from sklearn.naive_bayes import MultinomialNB
from sklearn.ensemble import RandomForestClassifier
from sklearn.svm import SVC
from sklearn.feature_extraction.text import (
    CountVectorizer,
    TfidfVectorizer,
)
from sklearn.feature_extraction import DictVectorizer
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import LabelEncoder, OneHotEncoder
from sklearn.metrics import classification_report
from typing import Union, Tuple, List, Set, Any
from base import (
    YiddishDiacritics,
    ContingencyTable,
    ClassificationInstanceCounter,
    Token,
    roman_letters_wpunc,
)
from feature_extraction import DefaultFeatureSetExtractor
import pandas as pd
import numpy as np
import string
import pickle
import click

RANDOM_SEED = 12345


def save_model(models: dict, vectorizer: Any, path: str) -> None:
    with open(path, "wb") as f:
        pickle.dump((models, vectorizer), f)


def prepare_data_multilabel(corpus):
    word_to_orth = (
        corpus.groupby("word").orthography.unique().apply(lambda l: set(l)).to_dict()
    )
    rows = [
        (
            w,
            "yivo" in orthographies,
            "romanized" in orthographies,
            "chasidic" in orthographies,
        )
        for w, orthographies in word_to_orth.items()
    ]
    output = pd.DataFrame.from_records(
        rows, columns=["word", "yivo", "romanized", "chasidic"]
    )
    return output


def get_classifier(classifier_type: str):
    return {
        "naive_bayes": MultinomialNB(),
        "random_forest": RandomForestClassifier(),
        "svm": SVC(),
        "svm_poly": SVC(kernel="poly"),
        "maxent": LogisticRegression(),
    }[classifier_type]


def classify_orthography(
    corpus_path: str,
    test_size: float,
    binarize_counts: bool = False,
    tfidf: bool = False,
    seed: int = RANDOM_SEED,
    interactive: bool = False,
    show_train_performance: bool = False,
    show_mistakes_yivo: bool = False,
    classifier: str = "naive_bayes",
    pickle_model: bool = False,
    model_output_path: str = "/dev/null",
) -> None:

    # load corpus
    raw_corpus = pd.read_csv(corpus_path, encoding="utf-8")

    # encode labels
    corpus = prepare_data_multilabel(raw_corpus)

    # train/test split
    train, test = train_test_split(
        corpus, test_size=test_size, shuffle=True, random_state=seed
    )

    # create features
    custom_extractor = DefaultFeatureSetExtractor()
    train_dict_feats = train.word.apply(lambda w: custom_extractor.extract(Token(w)))
    test_dict_feats = test.word.apply(lambda w: custom_extractor.extract(Token(w)))
    feature_transformer = DictVectorizer()

    classifiers = {}
    orthographies = ("yivo", "romanized", "chasidic")
    pretty_orthographies = ("YIVO", "Romanized", "Chasidic")
    for orth, pretty_orth in zip(orthographies, pretty_orthographies):

        # step 1: features
        X_train = feature_transformer.fit_transform(train_dict_feats)
        X_test = feature_transformer.transform(test_dict_feats)
        y_train = train[orth]

        # train NB classifier
        clf = get_classifier(classifier)
        clf.fit(X_train, y_train)
        classifiers[orth] = clf
        y_pred_train = clf.predict(X_train)
        y_pred_test = clf.predict(X_test)

        # classification report
        if show_train_performance:
            print(f"{pretty_orth} (train):")
            print(
                classification_report(
                    y_true=train[orth].astype(int), y_pred=y_pred_train
                )
            )
        print(f"{pretty_orth} (test):")
        print(classification_report(y_true=test[orth].astype(int), y_pred=y_pred_test))

        if show_mistakes_yivo and pretty_orth == "YIVO":
            counter = ClassificationInstanceCounter(
                instances=test.word,
                true_labels=test[orth].astype(int),
                predicted_labels=y_pred_test,
            )
            print("False positives")
            print("\n".join(counter.false_positives(label=1, n=10)))
            print("False negatives")
            print("\n".join(counter.false_negatives(label=1, n=10)))

    if interactive:
        user_word = Token(input("Please provide a word: "))
        user_word_features = custom_extractor.extract(user_word)
        print(f"Word: {user_word.text}")
        user_word_feat = feature_transformer.transform([user_word_features])
        for orth, pretty_orth in zip(orthographies, pretty_orthographies):
            clf = classifiers[orth]
            predicted_label = clf.predict(user_word_feat)
            print(f"{pretty_orth}?: {bool(predicted_label)}")

    if pickle_model:
        save_model(classifiers, feature_transformer, model_output_path)


CLASSIFIERS = set(("naive_bayes", "random_forest", "svm", "svm_poly", "maxent"))


@click.command()
@click.option("--corpus-path")
@click.option("--classifier")
@click.option("--test-size", type=float, default=0.2)
@click.option("--binarize-counts", is_flag=True)
@click.option("--tfidf", is_flag=True)
@click.option("--seed", default=RANDOM_SEED)
@click.option("--interactive", is_flag=True)
@click.option("--show-train-performance", is_flag=True)
@click.option("--show-mistakes-yivo", is_flag=True)
@click.option("--pickle-model", is_flag=True)
@click.option("--model-output-path")
def main(
    corpus_path: str,
    test_size: float,
    binarize_counts: bool,
    tfidf: bool,
    seed: int,
    interactive: bool,
    show_train_performance: bool,
    show_mistakes_yivo: bool,
    model_output_path: str,
    classifier: str = "naive_bayes",
    pickle_model: bool = False,
):

    if classifier not in CLASSIFIERS:
        raise ValueError("Unknown classifier!")

    classify_orthography(
        corpus_path,
        test_size,
        binarize_counts,
        tfidf,
        seed,
        interactive,
        show_train_performance,
        show_mistakes_yivo,
        classifier,
        pickle_model,
        model_output_path,
    )


if __name__ == "__main__":
    main()
