## Todo items
- bootstrap rule-based models for orth / etym id
    - noisy labels for SOME of the words
    - this becomes a missing-data problem in a way
- train semi-supervised model for the classification
- obtain necessary data
    - rest of wiktionary
    - in-geveb corpus 
    - yiddish wikipedia
- train language-models
    - how to get data for romanized orthography?
- perform orthograpahic classification
    - could this be done using language models? (bayes classifier style)
        p(orth | chars) = p(chars | orth) * p(orth)

## What you can do with this toolkit
- transliteration
    - translate freely between romanized/chasidic/yivo
    - this includes hebrew expansion as a sub-task
- spelling correction/canonicalization
- text classification
    - orthography detection
    - etymology detection
    - hebrew detection
- language modeling
    - score candidate strings
- subword segmentation
    - could use morfessor baseline at first?
- (sub)word embeddings
- ultimately ocr as well
